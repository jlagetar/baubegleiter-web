<!--- ACHTUNG BEACHTEN MOBILE MENU --->
<div class="left-sidebar-pro">
	<nav id="sidebar" class="">
		<div class="sidebar-header">
			<a href="start.php"><img class="main-logo" src="img/logo/logo.png" alt="" /></a>
			<strong><a href="start.php"><img src="img/logo/logosn.png" alt="" /></a></strong>
		</div>
		<div class="left-custom-menu-adp-wrap comment-scrollbar">
			<nav class="sidebar-nav left-sidebar-menu-pro">
				<ul class="metismenu" id="menu1">
					<li class="active">
						<a href="start.php">
							   <span class="educate-icon educate-home icon-wrap"></span>
							   <span class="mini-click-non">Startseite</span>
							</a>
					</li>
					<li>
						<a title="Landing Page" href="calendar.php" aria-expanded="false"><span class="educate-icon educate-event icon-wrap sub-icon-mg" aria-hidden="true"></span> <span class="mini-click-non">Kalender</span></a>
					</li>
					<li>
						<a href="timekeep.php" aria-expanded="false"><span class="educate-icon educate-data-table icon-wrap"></span> <span class="mini-click-non">Zeiterfassung</span></a>
					</li>
				<?php if($is_admin) { ?>
					<!--<li>
						<a href="#" aria-expanded="false"><span class="educate-icon educate-library icon-wrap"></span> <span class="mini-click-non">Auswertung</span></a>
					</li>-->
					<li>
						<a href="projects.php" aria-expanded="false"><span class="educate-icon educate-course icon-wrap"></span> <span class="mini-click-non">Projekte</span></a>
					</li>
					<li>
						<a href="administration.php" aria-expanded="false"><span class="educate-icon educate-professor icon-wrap"></span> <span class="mini-click-non">Benutzer</span></a>
					</li>
				<?php } ?>
				</ul>
			</nav>
		</div>
	</nav>
</div>