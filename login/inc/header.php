<link rel="apple-touch-icon" sizes="57x57" href="../apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="../apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="../apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="../apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="../apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="../apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="../apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="../apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="../apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="../android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="../favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="../favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="../favicon-16x16.png">
<link rel="manifest" href="../manifest.json">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="../ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff">


<link href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700,900" rel="stylesheet">
<link rel="stylesheet" href="css/bootstrap.min.css?v=2">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/owl.carousel.css">
<link rel="stylesheet" href="css/owl.theme.css">
<link rel="stylesheet" href="css/owl.transitions.css">
<link rel="stylesheet" href="css/animate.css">
<link rel="stylesheet" href="css/normalize.css">
<link rel="stylesheet" href="css/meanmenu.min.css">
<link rel="stylesheet" href="css/main.css">
<link rel="stylesheet" href="css/educate-custon-icon.css">
<link rel="stylesheet" href="css/morrisjs/morris.css">
<link rel="stylesheet" href="css/scrollbar/jquery.mCustomScrollbar.min.css">
<link rel="stylesheet" href="css/metisMenu/metisMenu.min.css">
<link rel="stylesheet" href="css/metisMenu/metisMenu-vertical.css">
<link rel="stylesheet" href="css/calendar/fullcalendar.min.css">
<link rel="stylesheet" href="css/calendar/fullcalendar.print.min.css">
<link rel="stylesheet" href="css/responsive.css">
<link rel="stylesheet" href="css/style.lagetar.css">
<link rel="stylesheet" href="style.css?v=2">
<script src="js/vendor/modernizr-2.8.3.min.js"></script>