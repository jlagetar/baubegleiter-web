<div class="footer-copyright-area">
	<div class="container-fluid">
		<div class="row">
			<div class="col-lg-12">
				<div class="footer-copy-right">
					<p>© 2017-<?php echo date('Y'); ?>. <a href="https://lagetar.com" target="_blank">Lagetar Solutions</a></p>
				</div>
			</div>
		</div>
	</div>
</div>