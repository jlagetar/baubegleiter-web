<?	include('_database.php');
	include('functions.php');
	
	session_cache_limiter('none');
	if (!isset($_SESSION)) {
    	session_start();
	}	
	
	ini_set('magic_quotes_gpc', 0);
	
	$email = $_POST['email'];
	
	$login_sql = "SELECT * from user WHERE email='".$email."'";
	if ($login_datas = $conn->query($login_sql)) {
		$login_data = $login_datas->fetch_assoc();
		if ($login_data === null || !isset($login_data)) {
			header("Location: index.php?email-error=1");
			exit();
		}
		
		if (bcrypt_sha384_verify($_POST['password'], $login_data['password'])) {
			if ($login_data['active'] == 0) {
				header("Location: index.php?user-inactive=1");
				exit();
			}
			
			$_SESSION["user_id"] = $login_data['id'];
			$_SESSION["user_name"] = $login_data['name'];
			$_SESSION["user_lastname"] = $login_data['lastname'];
			$_SESSION["user_email"] = $login_data['email'];
			$_SESSION["user_mobile"] = $login_data['mobile'];
			$_SESSION["user_is_admin"] = $login_data['is_admin'];
			$_SESSION["user_organization_id"] = $login_data['organization_id'];
			$token = openssl_random_pseudo_bytes(50); 
			$token = bin2hex($token);
			$_SESSION["token"] = $token;
			$sql_token = 'INSERT INTO user_token (user_id, token, created) VALUES('.$_SESSION["user_id"].', "'.$token.'", TIMESTAMP(NOW())) ON DUPLICATE KEY UPDATE token="'.$token.'", created=TIMESTAMP(NOW())';
			$conn->query($sql_token);
			
			header("Location: start.php");
			exit();
		}
		
		header("Location: index.php?password-error=1");
		exit();
	}
	
	header("Location: index.php?email-error=1");
	exit();
?>