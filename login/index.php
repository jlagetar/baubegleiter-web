
<script src="js/jquery.min.js"></script>
<link href="css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="js/bootstrap.min.js"></script>

<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
    <title>Baubegleiter</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">
	<link href="css/style.lagetar.css?v=2" rel="stylesheet" id="style-css">
</head>
<body>
    <div class="vertical-center" id="wrapper">
		<div class="container">
			<form class="form-horizontal" role="form" method="POST" action="login.php">
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4">
						<h2 style="color:white;">Baubegleiter Login</h2>
						<hr>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4">
						<div class="form-group has-danger">
							<label class="sr-only" for="email">E-Mail Addresse</label>
							<div class="input-group mb-2 mr-sm-2 mb-sm-0">
								<div class="input-group-addon"><i class="fa fa-at"></i></div>
								<input type="text" name="email" class="form-control" id="email"
									   placeholder="max@web.de" required autofocus>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-control-feedback">
							<span class="text-danger align-middle">
							<?php 	if (isset($_GET['email-error'])) {
										echo '<i class="fa fa-close"></i> Email-Adresse falsch';
									}
							?>
							</span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4">
						<div class="form-group">
							<label class="sr-only" for="password">Passwort</label>
							<div class="input-group mb-2 mr-sm-2 mb-sm-0">
								<div class="input-group-addon"><i class="fa fa-key"></i></div>
								<input type="password" name="password" class="form-control" id="password"
									   placeholder="Passwort" required>
							</div>
						</div>
					</div>
					<div class="col-md-4">						
					</div>
				</div>
				<div class="row" style="padding-top: 1rem">
					<div class="col-md-4"></div>
					<div class="col-md-4">
						<button type="submit" class="btn btn-success" style="background-color:#99c5f9;"><i class="fa fa-sign-in"></i> Anmelden</button>
						<a class="btn btn-link" href="password-reset.php" style="color:white;">Passwort vergessen?</a>
						
					</div>
					<div class="col-md-4"></div>
				</div>
				<?php 	if (isset($_GET['password-error']) || isset($_GET['user-inactive']) || isset($_GET['session-timeout'])) { ?>		
					<div class="row" style="padding-top: 1rem">
						<div class="col-md-4"></div>
						<div class="col-md-4">							
							<span class="text-danger align-middle">
							<?php 	if (isset($_GET['password-error'])) {
										echo '<i class="fa fa-close"></i> Passwort falsch';
									} else if (isset($_GET['user-inactive'])) {
										echo '<i class="fa fa-close"></i> Benutzer ist inaktiv';
									} else if (isset($_GET['session-timeout'])) {
										echo '<i class="fa fa-close"></i> Sitzung ist abgelaufen';
									}
							?> 
							</span>
						</div>
						<div class="col-md-4"></div>
					</div>
				<?php }?>
				<div class="row" style="padding-top: 1rem">
					<div class="col-md-4"></div>
					<div class="col-md-4">
						<a class="btn btn-link" href="../" style="color:white;">Zur&uuml;ck zur Webseite</a>
					</div>
					<div class="col-md-4"></div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>