<?php
	include('_database.php');
	include('functions.php');
	session_cache_limiter('none');
	if (!isset($_SESSION)) {
    	session_start();
	}
	
	if (!isset($_SESSION["user_id"]) || $_SESSION["user_id"] == null) {
		echo "";
		exit();
	}
	
	if($_SESSION["user_is_admin"] == 1) {
		$is_admin = true;
	}
	
	
	$WHERE = " WHERE user.id=".$_SESSION["user_id"];
	if ($is_admin) {
		$login_sql = "SELECT * from user WHERE id='".$_SESSION["user_id"]."'";
		if ($login_datas = $conn->query($login_sql)) {
			$login_data = $login_datas->fetch_assoc();
			$WHERE = "WHERE user.organization_id = ".$login_data['organization_id'];
		}
	}
	
echo '
{
	"success": 1,
	"result": [';
	
	$sql_events = "SELECT * FROM timekeepings LEFT JOIN events ON timekeepings.event_id=events.event_id LEFT JOIN user ON timekeepings.user_id = user.id ".$WHERE." order by timekeepings.time ASC";
	$color_array = [];
	$color_array[0] = 'event-warning';
	$color_array[1] = 'event-inverse';
	$color_array[2] = 'event-special';
	$color_array[3] = 'event-important';
	$color_array[4] = 'event-success';
	$color_array[5] = 'event-info';
	if ($events = $conn->query($sql_events)) {
		$i = 0;
		while($event = $events->fetch_assoc()) {
			if ($i > 0) { echo ','; } 
				$time = strtotime($event['time']) * 1000;
				$endtime = $time + 1800000;
				
				echo '{
					"id": "'.$event['timekeeping_id'].'",
					"title": "'.$event['time'].' - '.$event['name'].' '.$event['lastname'].': '.$event['text'].', '.$event['comment'].' ",
					"url": "#",
					"class": "'.$color_array[$event['user_id'] % 6].'",
					"start": "'.$time.'",
					"end":   "'.$endtime.'"
					}';
				
			$i = $i +1; 
		}
	}
echo '
	]
}';
?>