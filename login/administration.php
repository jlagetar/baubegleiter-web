<?php
	include('_database.php');
	include('_session.php');
	include('functions.php');
	
	$is_admin = false;
	$deleted = false;
	$invalid_data = false;
	$inserted = false;
	
	if($_SESSION["user_is_admin"] == 1) {
		$is_admin = true;
	} else {
		header("Location: start.php");
		exit();
	}
	
	if (isset($_POST['name']) && $_POST['name'] != "" && 
		isset($_POST['email']) && $_POST['email'] != ""  &&
		isset($_POST['lastname']) && $_POST['lastname'] != "" &&
		isset($_POST['password']) && $_POST['password'] != "") {
		$password = bcrypt_sha384_hash($_POST['password']);
		$user_sql = "INSERT into user (id,
											organization_id,
											name,
											lastname,
											password,
											last_login,
											is_admin,
											active,
											email,
											mobile) VALUES 
											(NULL, ".$_SESSION["user_organization_id"].",'".$_POST['name']."','".$_POST['lastname']."','".$password."',NULL,0,1,'".$_POST['email']."','".$_POST['mobile']."')";
		
		if ($conn->query($user_sql)) {
			$inserted = true;
		}
	} else if (isset($_GET['delete'])) {		
		$get_user_sql = "SELECT * FROM user WHERE id=".$_GET['delete'];
		if ($get_user = $conn->query($get_user_sql)) {
			$login_data = $get_user->fetch_assoc();
			$deleted_user = $login_data['name']." ".$login_data['lastname'];
		}
		
		$user_sql = "DELETE FROM user WHERE id=".$_GET['delete'];
		if ($conn->query($user_sql)) {
			$deleted = true;
		}
	} else {
		if (isset($_POST['name']) || 
			isset($_POST['email']) ||
			isset($_POST['lastname']) ||
			isset($_POST['password'])) {
			$invalid_data = true;
		}
	}
?>
<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Baubegleiter Benutzer</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include("inc/header.php"); ?>
</head>

<body>
    <!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
    <!-- Start Left menu area -->
    <? include ("inc/left_menu.php"); ?>
    <!-- End Left menu area -->
    <!-- Start Welcome area -->
    <div class="all-content-wrapper">
		<?php include("inc/top_menu.php"); ?>
        <div class="analytics-sparkle-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="container">
						<div class="row header-row" style="text-align:center;padding-bottom:2em;">
							<div class="col-md-4"></div>
							<div class="col-md-4">
								<h1 style="padding-top:2em;">Benutzerverwaltung</h1>
							</div>
							<div class="col-md-4"></div>
						</div>
						<table class="table table-condensed">
							<thead>
							  <tr>
								<th>Vorname</th>
								<th>Nachname</th>
								<th>Email</th>
								<th>Handy</th>
								<th>Status</th>
							  </tr>
							</thead>
							<tbody>
							  <?php $login_sql = "SELECT * from user WHERE id='".$_SESSION["user_id"]."'";
									if ($login_datas = $conn->query($login_sql)) {
										$login_data = $login_datas->fetch_assoc();
										$organization = $login_data['organization_id'];
										$login_sql = "SELECT * from user WHERE organization_id='".$organization."'";
										if ($login_datas = $conn->query($login_sql)) {
											while ($login_data = $login_datas->fetch_assoc()) {
												$delete = ' - <a href="administration.php?delete='.$login_data['id'].'">löschen</a>';
												if ($login_data['is_admin'] == 1) {
													$delete = '';
												}
												?>
											  <tr>
												<td><?php echo $login_data['name']; ?></td>
												<td><?php echo $login_data['lastname']; ?></td>
												<td><?php echo $login_data['email']; ?></td>
												<td><?php echo $login_data['mobile']; ?></td>
												<td><?php if ($login_data['active'] == 1) { echo 'aktiv'; } else { echo 'inaktiv'; } echo $delete; ?></td>
											  </tr>
											<?php }
										}
									}?>
							</tbody>
						</table>
						<?php if ($deleted) {?>
								<div class="row header-row">
									<div class="col-md-3"></div>
									<div class="col-md-6">
										<h3 style="text-align:center;color:green;">Benutzer <?php echo $deleted_user; ?> erfolgreich gelöscht!</h3>
									</div>
									<div class="col-md-3"></div>
								</div>
						<?php 
							}
						?>
						
						<form class="form-horizontal" role="form" method="POST" action="administration.php">
							<div class="row header-row">
								<div class="col-md-3"></div>
								<div class="col-md-6">
									<h2 style="text-align:center;">Neuen Benutzer hinzufügen</h2>
								</div>
								<div class="col-md-3"></div>
							</div>
						
							<div class="row">
								<div class="col-md-4"></div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="name">Vorname*</label>
										<input class="form-control" type="text" name="name" placeholder="Vorname" id="name">
									</div>
								</div>
								<div class="col-md-4"></div>
							</div>
							
							<div class="row">
								<div class="col-md-4"></div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="lastname">Nachname*</label>
										<input class="form-control" type="text" name="lastname" placeholder="Nachname" id="lastname">
									</div>
								</div>
								<div class="col-md-4"></div>
							</div>
					
							<div class="row">
								<div class="col-md-4"></div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="password">Passwort*</label>
										<input type="password" name="password" class="form-control" id="password" placeholder="Passwort" required>
									</div>
								</div>
								<div class="col-md-4"></div>
							</div>
							
							<div class="row">
								<div class="col-md-4"></div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="email">Email*</label>
										<input type="email" name="email" class="form-control" id="exampleInputEmail1" placeholder="Email">
									</div>
								</div>
								<div class="col-md-4"></div>
							</div>
							
							<div class="row">
								<div class="col-md-4"></div>
								<div class="col-md-4">
									<div class="form-group">
										<label for="mobile">Handy-Nr.</label>
										<input class="form-control" type="text" name="mobile" placeholder="+4912357689" id="mobile">
									</div>
								</div>
								<div class="col-md-4"></div>
							</div>
							
							<div class="row" style="padding-top: 1rem; padding-bottom:2em;">
								<div class="col-md-4"></div>
								<div class="col-md-4">
									<div  style="text-align:center;">
										<p>* Pflichtfelder</p>
										<button type="submit" class="btn btn-success" style="background-color:#99c5f9;">Neuen Benutzer hinzufügen</button>
									</div>
								</div>
								<div class="col-md-4">									
								</div>
							</div>	
							
							<?php  if ($inserted || $invalid_data) { ?>
								<div class="row" style="padding-top: 1rem; padding-bottom:2em;">
									<div class="col-md-4"></div>
									<div class="col-md-4" style="text-align:center;">
										
											<span class="text-success align-middle">
											<?php   if ($inserted) {
														echo 'Eintrag hinzugefügt.';
													}
													if ($invalid_data) {
														echo '<span style="color:red;">Bitte alle Pflichtfelder ausfüllen!</span>';
													}
											?>
											</span>
									</div>
									<div class="col-md-4">									
									</div>
								</div>
							<?php } ?>
						</form>
					</div>
                </div>
            </div>
        </div>
        <?php include ("inc/footer.php"); ?>
    </div>

    <?php include ("inc/scripts.php"); ?>
</body>

</html>