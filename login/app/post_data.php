<?php
	include('../_database.php');
	include('../functions.php');
	
	$token = $_POST['token'];
	$type = $_POST['type'];
	$user_id = $getUserIDFromTokenFunction($token);
	
	function base64_url_encode($input) {
		return strtr(base64_encode($input), '=/+', '._-');
	}

	function base64_url_decode($input) {
		return base64_decode(strtr($input, '._-', '=/+'));
	}
	
	if ($user_id == 0) {
		echo '[{"status": "invalid_token"}]';
		die();
	}
	
	if ($type == "image_upload") {
		$base= $_POST['image'];
		$name= $_POST['name'];
		
		//Now we need to get the correct Project_id by number_format
		$project_id = $_POST['project_id'];
		
		if ($project_id == 0) {
			echo '[{"status": "Incorrect Project ID"}]';
			die();
		}		
		
		/*
		$filetext = 'data.txt';
		$current = file_get_contents($filetext);
		// Append a new person to the file
		$fileWrite = strtr($base, '._-', '=/+');
		$current .= "##".$fileWrite."##";
		// Write the contents back to the file
		file_put_contents($filetext, $current);*/
		
		
		$binary = base64_url_decode($base);
		if (strlen($binary) > 0) {
			if (!file_exists("../project_images/".$project_id)) {
				mkdir("../project_images/".$project_id, 0777, true);
			}
			
			$file = fopen("../project_images/".$project_id."/".$name, 'wb');
			fwrite($file, $binary);
			fclose($file);
			
			$sql_insert = "INSERT INTO project_pictures (picture_id, project_id, path, time) VALUES (NULL, ".$project_id.",'".$name."', CURRENT_TIMESTAMP)";
			if ($conn->query($sql_insert)) {
				echo '[{"status": "ok"}]';
			} else {
				echo '[{"status": "SQL insert failed"}]';
			}
		} else {
			echo '[{"status": "Base64 Error"}]';
		}
	} else if ($type == "timekeep") {
		$project_number = $_POST['projectId'];
		$project_id = $_POST['projectId'];
		$get_projects_sql = "SELECT project_id FROM `projects` LEFT JOIN user ON user.organization_id = projects.organization_id WHERE projects.project_number = '".$project_number."' AND user.id =".$user_id;

		if ($get_projects = $conn->query($get_projects_sql)) {
			if ($project = $get_projects->fetch_assoc()) {
				$project_id = $project['project_id'];
			}
		}		
		
		$timekeep_sql = "INSERT into timekeepings (timekeeping_id,user_id,event_id,time,comment,duration, project_id, added_time) VALUES (NULL, ".$user_id.",".$_POST['eventtype'].",'".$_POST['time']."','".$_POST['comment']."',0,".$project_id.", NULL)";	
		if ($conn->query($timekeep_sql)) {
			echo '[{"status": "ok"}]';
		} else {		
			echo '[{"status": "SQL insert failed"}]';
		}
	} else {
		echo '[{"status": "invalid_type"}]';
	}
	
	die();
?>