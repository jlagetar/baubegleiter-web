<?php
	include('../_database.php');
	include('../functions.php');
	
	$tempData = stripslashes(html_entity_decode($_GET['jsondata']));
	$jsonarray = json_decode($tempData, true);
	
	function decrypt($string) {
		$encrypt_method = "AES-256-CBC";
		$secret_key = "69ba594657c6ba1e08763f73c204b40f";
		$secret_iv = "a7c0238345bf7efa";
		return openssl_decrypt(base64_url_decode($string), $encrypt_method, $secret_key, OPENSSL_RAW_DATA, $secret_iv);
	}
	
	function base64_url_encode($input) {
		return strtr(base64_encode($input), '=/+', '._-');
	}

	function base64_url_decode($input) {
		return base64_decode(strtr($input, '._-', '=/+'));
	}
	
	$type = $jsonarray['type'];
	if ($type != "get_token") {
		$token = $jsonarray['token'];
		$user_id = $getUserIDFromTokenFunction($token);
		
		if ($user_id == 0) {
			echo '[{"status": "invalid_token"},{"projects": []}]';
			die();
		}
	}
	
	if ($type == "projects") {
		$success_message = "ok";
		$time = $jsonarray['time'];
		$return_val = array();
		$get_projects_sql = "SELECT *, projects.name as projectname FROM `projects` LEFT JOIN user ON user.organization_id = projects.organization_id WHERE timestamp > '".$time."' AND user.id =".$user_id;
		$projects = array();
		if ($get_projects = $conn->query($get_projects_sql)) {
			while ($project = $get_projects->fetch_assoc()) {
				$proj = array(	'id' => $project['project_id'],
								'number' => $project['project_number'],
								'name' => $project['projectname'],
								'customer' => $project['customer']);
				array_push($projects, $proj);
			}
		}		
		
		$status = array('status' => $success_message);
		$all_projects = array('projects' => $projects);
		array_push($return_val, $status);
		array_push($return_val, $all_projects);
		$post_data = json_encode($return_val, JSON_UNESCAPED_UNICODE);
		echo $post_data;
	} else if ($type == "project_images") {
		$time = $jsonarray['time'];
		$project_number = $jsonarray['project_id'];
		$project_id = $jsonarray['project_id'];
		$get_projects_sql = "SELECT project_id FROM `projects` LEFT JOIN user ON user.organization_id = projects.organization_id WHERE projects.project_number = '".$project_number."' AND user.id =".$user_id;
		if ($get_projects = $conn->query($get_projects_sql)) {
			if ($project = $get_projects->fetch_assoc()) {
				$project_id = $project['project_id'];
			}
		}
		$success_message = "ok";
		$return_val = array();
		$get_projects_sql = "SELECT * FROM `project_pictures` WHERE project_id = ".$project_id." AND time > '".$time."'";
		$projects = array();
		if ($get_projects = $conn->query($get_projects_sql)) {
			while ($project = $get_projects->fetch_assoc()) {
				$proj = array(	'id' => $project['picture_id'],
								'path' => $project['path'],
								'project_id' => $project_id);
				array_push($projects, $proj);
			}
		}		
		
		$status = array('status' => $success_message);
		$all_projects = array('project_images' => $projects);
		array_push($return_val, $status);
		array_push($return_val, $all_projects);
		$post_data = json_encode($return_val, JSON_UNESCAPED_UNICODE);
		echo $post_data;
	} else if ($type == "timkeepings") {
		$time = $jsonarray['time'];
		$success_message = "ok";
		$return_val = array();
		$sql_events = "SELECT * FROM timekeepings LEFT JOIN events ON timekeepings.event_id=events.event_id LEFT JOIN user ON timekeepings.user_id = user.id LEFT JOIN projects ON projects.project_id = timekeepings.project_id WHERE user.id=".$user_id." AND added_time >= '".$time."' order by timekeepings.time ASC";

		$timekeepings = array();
		if ($get_events = $conn->query($sql_events)) {
			while ($timekeep_data = $get_events->fetch_assoc()) {
				$timekeep = array('event_id' => $timekeep_data['event_id'],
								  'time' => $timekeep_data['time'],
								  'comment' => $timekeep_data['comment'],
								  'duration' => $timekeep_data['duration'],
								  'project_id' => $timekeep_data['project_number'],
								  'timekeeping_id' => $timekeep_data['timekeeping_id']);
				array_push($timekeepings, $timekeep);
			}
		}
		
		$status = array('status' => $success_message);
		$all_timkeeps = array('timekeepings' => $timekeepings);
		array_push($return_val, $status);
		array_push($return_val, $all_timkeeps);
		$post_data = json_encode($return_val, JSON_UNESCAPED_UNICODE);
		echo $post_data;
	} else if ($type == "get_token") {
		$user = $jsonarray['username'];
		$password = $jsonarray['password'];
		$check_password = decrypt($password);
		$sql = "SELECT * from user WHERE email='".$user."'";
		if ($get_password = $conn->query($sql)) {
			$password = $get_password->fetch_assoc();
			if (bcrypt_sha384_verify($check_password, $password['password'])) {
				//THIS IS A VALID USER, RETURN TOKEN
				$sql_token_get = "SELECT * from user_token WHERE user_id='".$password['id']."' AND created >= NOW() - INTERVAL 1 DAY";
				$final_token = "";
				$expires = "";
				if ($get_token = $conn->query($sql_token_get)) {
					if ($token = $get_token->fetch_assoc()) {
						$final_token = $token['token'];
						$expires_date = new DateTime(($token['created']));
						$expires_date->modify('+1 day');
						$expires = $expires_date->format('Y-m-d H:i:s');
					}
				} 

				if ($final_token == "") {				
					//IF TOKEN INVALID
					$token = openssl_random_pseudo_bytes(50); 
					$token = bin2hex($token);
					$_SESSION["token"] = $token;
					$sql_token = 'INSERT INTO user_token (user_id, token, created) VALUES('.$password['id'].', "'.$token.'", TIMESTAMP(NOW())) ON DUPLICATE KEY UPDATE token="'.$token.'", created=TIMESTAMP(NOW())';
					$conn->query($sql_token);
					$final_token = $token;
					$expires = date('Y-m-d H:i:s');
				}
				
				$status = array('status' => "ok");
				$token_arr = array('token' => $final_token);
				$expires_arr = array('expires' => $expires);
				$return_val = array();
				array_push($return_val, $status);
				array_push($return_val, $token_arr);
				array_push($return_val, $expires_arr);
				$post_data = json_encode($return_val, JSON_UNESCAPED_UNICODE);
				echo $post_data;
			}
		}
	} else {
		echo '[{"status": "invalid_type"},{"projects": []}]';
	}
	
	die();
?>