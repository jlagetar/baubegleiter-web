<?php
	include('_database.php');
	include('functions.php');
	
	$token = $_GET['token'];
	$is_valid_token = false;
	
	session_cache_limiter('none');
	if (!isset($_SESSION)) {
    	session_start();
	}
	
	if (!isset($_SESSION['token'])) {
		$_SESSION['token'] = $token;
	} else {
		$token = $_SESSION['token'];
	}
	
	$password_sql = "SELECT * from password_reset WHERE token='".$token."'";
	if ($password_datas = $conn->query($password_sql)) {
		$password_data = $password_datas->fetch_assoc();
		if (isset($password_data['user_id'])) {
			$is_valid_token = true;
			$user_id = $password_data['user_id'];
		}
	}
	
	if (isset($_POST['password']) || isset($_POST['password2'])) {
		if (strcmp($_POST['password'], $_POST['password2']) == 0) {
			//Password is valid -> set to user, delete entry by token from database, and show it to the user that it is finished
			$sql_set_new_password = "UPDATE user SET password='".bcrypt_sha384_hash($_POST['password'])."' WHERE id=".$user_id;
			$conn->query($sql_set_new_password);
			
			$sql_delete_password_reset = "DELETE FROM password_reset WHERE token='".$token."'";
			$conn->query($sql_delete_password_reset);
			$password_set_finished = true;
		} else {
			$wrong_password = true;
		}
	}
	
	if (!$is_valid_token) {
		header("Location: index.php");
		exit();
	} else {
?>

<link href="css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.js"></script>

<!DOCTYPE html>
<html lang="de">
<head>
    <!-- Standard Meta -->
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <!-- Site Properties -->
    <title>Baubegleiter - Passwort vergessen</title>

    <!-- Stylesheets -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">	
	<link href="css/style.lagetar.css" rel="stylesheet" id="style-css">
</head>
<body>
    <div class="vertical-center" id="wrapper">
		<div class="container">
			<form class="form-horizontal" role="form" method="POST" action="password-reset-return.php">
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4">
						<h2 style="color:white;">Passwort zurücksetzen</h2>
						<h4 style="color:white;">Neues Passwort vergeben</h4>
						<hr>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4">
						<div class="form-group has-danger">
							<label class="sr-only" for="password">Passwort</label>
							<div class="input-group mb-2 mr-sm-2 mb-sm-0">
								<div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-key"></i></div>
								<input type="password" name="password" class="form-control" id="password"
									   placeholder="Passwort" required>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-control-feedback">
							<span class="text-danger align-middle">
							<?php 	if (isset($wrong_password)) {
										echo '<i class="fa fa-close"></i> Passwort falsch oder nicht übereinstimmend';
									}
							?>
							</span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4">
						<div class="form-group has-danger">
							<label class="sr-only" for="password2">Passwort bestätigen</label>
							<div class="input-group mb-2 mr-sm-2 mb-sm-0">
								<div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-key"></i></div>
								<input type="password" name="password2" class="form-control" id="password2"
									   placeholder="Passwort" required>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-control-feedback">
							<span class="text-success align-middle">
							<?php if (isset($password_set_finished)) {
										echo 'Passwort erfolgreich gesetzt.<a href="index.php">Zur Anmeldung</a>';
									}
							?>
							</span>
						</div>
					</div>
				</div>
				<div class="row" style="padding-top: 1rem">
					<div class="col-md-4"></div>
					<div class="col-md-4">
						<button type="submit" class="btn btn-success" style="background-color:#99c5f9;"><i class="fa fa-sign-in"></i> Passwort zurücksetzen</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>
<?php } ?>