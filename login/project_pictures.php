<?php
	include('_database.php');
	include('_session.php');
	include('functions.php');
	
	$is_admin = false;
	
	if($_SESSION["user_is_admin"] == 1) {
		$is_admin = true;
	}
	
	$token = $_GET['token'];
	$id = $_GET['id'];
	$user_id = $getUserIDFromTokenFunction($token);
	$has_access = true;
	
	if ($user_id == 0) {
		$has_access = false;
	}
?>

<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Baubegleiter Startseite</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include("inc/header.php"); ?>
</head>

<body>
    <!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
    <!-- Start Left menu area -->
    <? include ("inc/left_menu.php"); ?>
    <!-- End Left menu area -->
    <!-- Start Welcome area -->
    <div class="all-content-wrapper">
		<?php include("inc/top_menu.php"); ?>
        <div class="analytics-sparkle-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12" style="text-align:center;">
						<h1 style="padding-top:2em;padding-bottom:2em;">Projekt Bilder</h1>
						<a href="projects.php">Zur&uuml;ck zur Projekt-Übersicht</a><br/><br/>
						<?php
							if (isset($id) && is_dir("project_images/".$id) && !empty(scandir("project_images/".$id)) && $has_access) {
								$files = array_diff(scandir("project_images/".$id), array('.', '..'));
								
								foreach ($files as $file) {
									//Now check if this file is in the database
									$sql_pictures = "SELECT * FROM `project_pictures` WHERE project_id=".$id." AND path='".$file."'";
									if ($pictures = $conn->query($sql_pictures)) {
										if ($picture = $pictures->fetch_assoc()) {
											$pic_id = $picture['picture_id'];
											if ($pic_id > 0) {
												echo '<img src="project_images/'.$id.'/'.$file.'" />&nbsp;';	
											}
										}
									}
								}
							} else {
								echo "<h1>Keine Bilder zu diesem Projekt vorhanden.</h1>";
							}
						?>
                    </div>
                </div>
            </div>
        </div>
        <?php include ("inc/footer.php"); ?>
    </div>

    <?php include ("inc/scripts.php"); ?>
</body>

</html>