<?php
	include('_database.php');
	include('_session.php');
	include('functions.php');
	
	$is_admin = false;
	
	if($_SESSION["user_is_admin"] == 1) {
		$is_admin = true;
	}
	
	if (isset($_POST['number']) && isset($_POST['name']) && isset($_POST['customer']) && !empty($_POST['number']) && !empty($_POST['name']) && !empty($_POST['customer'])) {
		$projects_sql = "INSERT into projects (project_id,project_number,name,customer,active,timestamp, organization_id) 
									VALUES (NULL, '".$_POST['number']."','".$_POST['name']."','".$_POST['customer']."', 1, NULL, ".$_SESSION["user_organization_id"].")";
		
		if ($conn->query($projects_sql)) {
			$inserted = true;
		}
	} else if (isset($_GET['delete'])) {
		$delete_sql = "DELETE FROM projects WHERE project_id=".$_GET['project_id'];
		if ($conn->query($delete_sql)) {
			$deleted = true;
		}
	} else if (!empty($_POST)) {
		$invalid_data = true;
	}
?>

<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Baubegleiter Projekte</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include("inc/header.php"); ?>
</head>

<body>
    <!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
    <!-- Start Left menu area -->
    <? include ("inc/left_menu.php"); ?>
    <!-- End Left menu area -->
    <!-- Start Welcome area -->
    <div class="all-content-wrapper">
		<?php include("inc/top_menu.php"); ?>
        <div class="analytics-sparkle-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
						<div class="container">
							<form class="form-horizontal" role="form" method="POST" action="projects.php">
								<div class="row header-row">
									<div class="col-md-4"></div>
									<div class="col-md-4">
										<h1 style="text-align:center;padding-top:2em;">Projekte</h1>
									</div>
									<div class="col-md-4"></div>
								</div>
							
								<div class="row">
									<div class="col-md-4"></div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Projekt Nummer</label>
											<input class="form-control" type="text" value="<?php if (!$inserted) {echo $_POST['number'];} ?>" id="number" name="number">
										</div>
									</div>
									<div class="col-md-4"></div>
								</div>
								
								<div class="row">
									<div class="col-md-4"></div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Projekt Name</label>
											<input class="form-control" type="text" value="<?php if (!$inserted) {echo $_POST['name'];} ?>" id="name" name="name">
										</div>
									</div>
									<div class="col-md-4"></div>
								</div>
								
								<div class="row">
									<div class="col-md-4"></div>
									<div class="col-md-4">
										<div class="form-group">
											<label>Endkunden Name</label>
											<input class="form-control" type="text" value="<?php if (!$inserted) {echo $_POST['customer'];} ?>" id="customer" name="customer">
										</div>
									</div>
									<div class="col-md-4"></div>
								</div>
								
								<div class="row" style="padding-top: 1rem">
									<div class="col-md-4"></div>
									<div class="col-md-4">
										<div style="text-align:center">
											<button type="submit" class="btn btn-success" style="background-color:#99c5f9;">Neues Projekt hinzufügen</button>
										</div>
										<div class="form-control-feedback" style="text-align:center;">
											<span class="text-success align-middle">
											<?php if (isset($inserted)) {
														echo 'Eintrag hinzugefügt.';
													}
											?>
											</span>
											<span class="text-error align-middle">
											<?php if (isset($invalid_data)) {
														echo 'Felder unvollständig ausgefüllt.';
													}
											?>
											</span>
										</div>
									</div>
									<div class="col-md-4"></div>
								</div>

								<div class="row" style="padding-top: 1rem">
								<?php
									$get_projects_sql = "SELECT * FROM `projects` WHERE organization_id = ".$_SESSION["user_organization_id"];
									$i = 0;
									if ($get_projects = $conn->query($get_projects_sql)) {
										while ($project = $get_projects->fetch_assoc()) {
											if ($i == 0) {
												?>
												<div style="width:100%;text-align:center; padding-bottom:1em;">
													<div style="padding-top:5em;">
														<h3>Bestehende Projekte</h3>
													</div>
												</div>
												<div style="clear:both;"></div>
												<table style="margin: 0 auto;">
												<tr><th width="150">Nummer</th><th width="150">Name</th><th width="150">Endkunde</th><th width="150">Löschen</th><th width="150">Bilder</th></tr>
												<?
											}?>
											
											<tr><td><?php echo $project['project_number'];?></td><td><?php echo $project['name'];?></td><td><?php echo $project['customer'];?></td><td><a href="projects.php?delete=1&project_id=<?php echo $project['project_id']; ?>">löschen</a></td><td><a href="<?php echo "project_pictures.php?token=".$_SESSION["token"]."&id=".$project['project_id'];?>"> ansehen</a></td></tr> 
											
										<?php
											$i++;
										}
										
										if ($i > 0) {
											?></table><?
										}
									}
									
									
								?>
								</div>
								<div class="row" style="padding-top: 1rem;padding-bottom:2em;text-align:center;">
									<div class="col-md-4"></div>
									<div class="col-md-4">
										<?php
											if ($deleted) {
												echo '<p style="text-align:center;color:green;">Projekt wurde erfolgreich gelöscht!</p>';
											}
										?>
									</div>
									<div class="col-md-4"></div>				
								</div>
							</form>
						</div>
                    </div>
                </div>
            </div>
        </div>
        <?php include ("inc/footer.php"); ?>
    </div>

    <?php include ("inc/scripts.php"); ?>
</body>
</html>