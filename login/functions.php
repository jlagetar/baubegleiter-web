<?php
	/**
	 * Bcrypt-SHA-384 Verification
	 * 
	 * @ref http://stackoverflow.com/a/36638120/2224584
	 * @param string $plaintext
	 * @param string $Hash
	 * @return bool
	 */
	function bcrypt_sha384_verify($plaintext, $hash)
	{
		$prehash = base64_encode(hash('sha384', $plaintext, true));
		return password_verify($prehash, $hash);
	}

	/**
	 * Creates a Bcrypt-SHA-384 hash
	 * 
	 * @ref http://stackoverflow.com/a/36638120/2224584
	 * @param string $plaintext
	 * @param int $cost
	 * @return string
	 */
	function bcrypt_sha384_hash($plaintext)
	{
		$prehash = base64_encode(hash('sha384', $plaintext, true));
		return password_hash($prehash, PASSWORD_BCRYPT, ['cost' => 12]);
	}
?>