<?php
	include('_database.php');
	include('_session.php');
	include('functions.php');
	
	$is_admin = false;
	
	if($_SESSION["user_is_admin"] == 1) {
		$is_admin = true;
	}
	
	if (isset($_POST['eventtype'])) {
		$user_id = $_SESSION["user_id"];
		if ($is_admin) {
			$user_id = $_POST['user_id'];
		}
		
		$time = $_POST['time'];
		$parts = explode(" ", $time);
		$date = $parts[0];
		$timepart = $parts[1];
		$dateParts = explode(".", $date);
		$time = $dateParts[2]."-".$dateParts[1]."-".$dateParts[0]." ".$timepart;
		
		$timekeep_sql = "INSERT into timekeepings (timekeeping_id,user_id,event_id,time,comment,duration, project_id, added_time) VALUES (NULL, ".$user_id.",".$_POST['eventtype'].",'".$time."','".$_POST['comment']."',0,".$_POST['project_id'].", NULL)";

		if ($conn->query($timekeep_sql)) {
			$inserted = true;
		}
	}
?>

<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Baubegleiter Zeiterfassung</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include("inc/header.php"); ?>
	<link href="css/datetimepicker.css" rel="stylesheet" id="datetime-css">
</head>

<body>
    <!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
    <!-- Start Left menu area -->
    <? include ("inc/left_menu.php"); ?>
    <!-- End Left menu area -->
    <!-- Start Welcome area -->
    <div class="all-content-wrapper">
		<?php include("inc/top_menu.php"); ?>
        <div class="analytics-sparkle-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
						
						<div class="container">
							<form class="form-horizontal" role="form" method="POST" action="timekeep.php">
								<div class="row header-row">
									<div class="col-md-4"></div>
									<div class="col-md-4" style="text-align:center;">
										<h1 style="padding-top:2em;">Zeiterfassung</h1>
									</div>
									<div class="col-md-4"></div>
								</div>
								
								<?php if($is_admin) { ?>
								<div class="row">
									<div class="col-md-4"></div>
									<div class="col-md-4">
										<div class="form-group">
											<label for="eventtype">Für Benutzer</label>
											<select class="form-control" id="user_id" name="user_id">
											 <?php $sql_users = "SELECT * from `user` WHERE organization_id = ".$_SESSION["user_organization_id"];
													if ($users = $conn->query($sql_users)) {
														while ($user = $users->fetch_assoc()) {
															echo '<option value="'.$user['id'].'">'.$user['name']." ".$user['lastname'].'</option>';
														}
													}
											?>
											</select>
										</div>
									</div>
									<div class="col-md-4"></div>
								</div>
								<?php }	?>
								
								<div class="row">
									<div class="col-md-4"></div>
									<div class="col-md-4">
										<div class="form-group">
											<label for="eventtype">Projekt</label>
											<select class="form-control" id="project_id" name="project_id">
												<option value="0">Allgemein</option>
											 <?php $sql_projects = "SELECT * from `projects` WHERE organization_id = ".$_SESSION["user_organization_id"];
													if ($projects = $conn->query($sql_projects)) {
														while ($project = $projects->fetch_assoc()) {
															echo '<option value="'.$project['project_id'].'">'.$project['name']."-".$project['customer'].'</option>';
														}
													}
											?>
											</select>
										</div>
									</div>
									<div class="col-md-4"></div>
								</div>
								<div class="row">
									<div class="col-md-4"></div>
									<div class="col-md-4">
										<div class="form-group">
											<label for="eventtype">Ereignis</label>
											<select class="form-control" id="eventtype" name="eventtype">
											 <?php $sql_events = "SELECT * from events ORDER BY event_id";
													if ($event_datas = $conn->query($sql_events)) {
														while ($event_data = $event_datas->fetch_assoc()) {
															echo '<option value="'.$event_data['event_id'].'">'.$event_data['text'].'</option>';
														}
													}
											?>
											</select>
										</div>
									</div>
									<div class="col-md-4"></div>
								</div>
								<div class="row">
									<div class="col-md-4"></div>
									<div class="col-md-4">
										<div class="form-group">
											<label for="comment">Kommentar</label>
											<textarea class="form-control" id="comment" rows="3" name="comment"></textarea>
										</div>
									</div>
									<div class="col-md-4"></div>
								</div>
								<div class="row">
									<div class="col-md-4"></div>
									<div class="col-md-4">
										<div class="form-group">
										<label for="time">Zeitpunkt</label>
											<div class="input-group date" id="datetimepicker2">							
												<input type="text" class="form-control" value="<?php echo date('Y-m-d')." ".date('H:i:s'); ?>" id="time" name="time"/>
												<span class="input-group-addon" style="width: 1em;">
													<span class="glyphicon glyphicon-calendar"></span>
												</span>							
											</div>
										</div>					
										
										<!--<input class="form-control" type="datetime-local" value="<?php echo date('Y-m-d')."T".date('H:i:s'); ?>" id="time" name="time">-->
										<!--<input class="form-control" type="text" value="<?php echo date('Y-m-d')." ".date('H:i:s'); ?>" id="time" name="time">-->

									</div>
									<div class="col-md-4"></div>
								</div>
								<div class="row" style="padding-top: 1rem; padding-bottom:2em;">
									<div class="col-md-4"></div>
									<div class="col-md-4" style="text-align:center;">
										<div >
											<button type="submit" class="btn btn-success" style="background-color:#99c5f9;">Zeit Eintragen</button>
										</div>
										<div style="padding-top:1em;padding-bottom:1em;">
											<span class="text-success align-middle">
												<?php if (isset($inserted)) {
															echo 'Eintrag hinzugefügt.';
														}
												?>
											</span>
										</div>
									</div>
									<div class="col-md-4">
									</div>
								</div>		
							</form>
						</div>
                    </div>
                </div>
            </div>
        </div>
        <?php include ("inc/footer.php"); ?>
    </div>

    <?php include ("inc/scripts.php"); ?>
	<script src="js/moment.js"></script>
	<script src="js/datetimepicker.js?v=2"></script>
	<script type="text/javascript">
		$( document ).ready(function() {
			$('#datetimepicker2').datetimepicker({
				locale: 'de'
			});
		});
	</script>
</body>

</html>