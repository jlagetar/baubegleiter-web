<?php
	include('_database.php');
	
	function getToken($length){
		 $token = "";
		 $codeAlphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		 $codeAlphabet.= "abcdefghijklmnopqrstuvwxyz";
		 $codeAlphabet.= "0123456789";
		 $max = strlen($codeAlphabet); // edited

		for ($i=0; $i < $length; $i++) {
			$token .= $codeAlphabet[rand(0, $max-1)];
		}

		return $token;
	}
	
	function url_origin( $s, $use_forwarded_host = false )
	{
		$ssl      = ( ! empty( $s['HTTPS'] ) && $s['HTTPS'] == 'on' );
		$sp       = strtolower( $s['SERVER_PROTOCOL'] );
		$protocol = substr( $sp, 0, strpos( $sp, '/' ) ) . ( ( $ssl ) ? 's' : '' );
		$port     = $s['SERVER_PORT'];
		$port     = ( ( ! $ssl && $port=='80' ) || ( $ssl && $port=='443' ) ) ? '' : ':'.$port;
		$host     = ( $use_forwarded_host && isset( $s['HTTP_X_FORWARDED_HOST'] ) ) ? $s['HTTP_X_FORWARDED_HOST'] : ( isset( $s['HTTP_HOST'] ) ? $s['HTTP_HOST'] : null );
		$host     = isset( $host ) ? $host : $s['SERVER_NAME'] . $port;
		return $protocol . '://' . $host;
	}

	function full_url( $s, $use_forwarded_host = false )
	{
		return url_origin( $s, $use_forwarded_host ) . $s['REQUEST_URI'];
	}

	if (isset($_POST['email']) || isset($_POST['email2'])) {
		if (strcmp($_POST['email'], $_POST['email2']) == 0) {
			$password_sql = "SELECT * from user WHERE email='".$_POST['email']."'";
			if ($password_datas = $conn->query($password_sql)) {
				$password_data = $password_datas->fetch_assoc();
				if (isset($password_data['password'])) {
					$correct_email = true;
					$token = getToken(50);
					$password_set_reset_sql = "INSERT INTO password_reset (id, time, user_id, token) VALUES (NULL,NULL, '".$password_data['id']."','".$token."')";
					$conn->query($password_set_reset_sql);
					
					$absolute_url = full_url( $_SERVER );
					$reset_url = str_replace("password-reset.php", "password-reset-return.php", $absolute_url);
					$empfaenger = $_POST['email'];
					$betreff = 'Baubegleiter Passwort anpassen';
					$nachricht = 'Hallo, um dein Passwort anzupassen, folge bitte folgenden Link: '.$reset_url."?token=".$token;
					$header = 'From: baubegleiter@lagetar.com' . "\r\n" .
						'Reply-To: baubegleiter@lagetar.com' . "\r\n" .
						'X-Mailer: PHP/' . phpversion();

					mail($empfaenger, $betreff, $nachricht, $header);
					mail("jozo@lagetar.com", $betreff, $nachricht, $header);
				} else {
					$wrong_email = true;
				}
			} else {
				$wrong_email = true;
			}
		} else {
			$wrong_email = true;
		}
	}
?>

<link href="css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery.js"></script>

<!DOCTYPE html>
<html lang="de">
<head>
    <!-- Standard Meta -->
    <meta charset="utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">

    <!-- Site Properties -->
    <title>Baubegleiter - Passwort vergessen</title>

    <!-- Stylesheets -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.1/css/font-awesome.min.css">	
	<link href="css/style.lagetar.css" rel="stylesheet" id="style-css">
</head>
<body>
    <div class="vertical-center" id="wrapper">
		<div class="container">
			<form class="form-horizontal" role="form" method="POST" action="password-reset.php">
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4">
						<h2 style="color:white;">Passwort vergessen</h2>
						<h4 style="color:white;">Email-Adresse eingeben zum Zurücksetzen</h4>
						<hr>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4">
						<div class="form-group has-danger">
							<label class="sr-only" for="email">E-Mail Addresse</label>
							<div class="input-group mb-2 mr-sm-2 mb-sm-0">
								<div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-at"></i></div>
								<input type="text" name="email" class="form-control" id="email"
									   placeholder="max@web.de" required autofocus>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-control-feedback">
							<span class="text-danger align-middle">
							<?php 	if (isset($wrong_email)) {
										echo '<i class="fa fa-close"></i> Email-Adresse falsch oder nicht übereinstimmend';
									}
							?>
							</span>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4"></div>
					<div class="col-md-4">
						<div class="form-group has-danger">
							<label class="sr-only" for="email">E-Mail Addresse bestätigen</label>
							<div class="input-group mb-2 mr-sm-2 mb-sm-0">
								<div class="input-group-addon" style="width: 2.6rem"><i class="fa fa-at"></i></div>
								<input type="text" name="email2" class="form-control" id="email2"
									   placeholder="max@web.de" required autofocus>
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="form-control-feedback">
							<span class="text-success align-middle">
							<?php if (isset($correct_email)) {
										echo 'Link zum Zurücksetzen des Passworts an Email-Adresse versendet.';
									}
							?>
							</span>
						</div>
					</div>
				</div>
				<div class="row" style="padding-top: 1rem">
					<div class="col-md-4"></div>
					<div class="col-md-4">
						<button type="submit" class="btn btn-success" style="background-color:#99c5f9;"><i class="fa fa-sign-in"></i> Passwort zurücksetzen</button>
					</div>
				</div>
			</form>
		</div>
	</div>
</body>
</html>