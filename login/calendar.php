<?php
	include('_database.php');
	include('_session.php');
	include('functions.php');
	
	$is_admin = false;
	
	if($_SESSION["user_is_admin"] == 1) {
		$is_admin = true;
	}
?>

<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Baubegleiter Kalender</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include("inc/header.php"); ?>
	<link rel="stylesheet" href="css/calendar.css?v=2">
</head>

<body>
    <!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
    <!-- Start Left menu area -->
    <? include ("inc/left_menu.php"); ?>
    <!-- End Left menu area -->
    <!-- Start Welcome area -->
    <div class="all-content-wrapper">
		<?php include("inc/top_menu.php"); ?>
        <div class="analytics-sparkle-area" style="padding-bottom:3em;">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">
						<div class="container">
							<div class="row">
								<div class="span12" style="text-align:center;padding-top:2em;padding-bottom:1em;">
									<div class="btn-group">
										<button class="btn btn-primary" data-calendar-nav="prev"><< Zur&uuml;ck</button>
										<button class="btn" data-calendar-nav="today">Heute</button>
										<button class="btn btn-primary" data-calendar-nav="next">Weiter >></button>
									</div>
									<div class="btn-group">
										<button class="btn btn-warning" data-calendar-view="year">Jahr</button>
										<button class="btn btn-warning active" data-calendar-view="month">Monat</button>
										<button class="btn btn-warning" data-calendar-view="week">Woche</button>
										<button class="btn btn-warning" data-calendar-view="day">Tag</button>
									</div>
								</div>
							</div>

							<div class="row">
								<div class="span12">
									<div id="calendar"></div>
								</div>
							</div>

							<div class="clearfix"></div>							

							
						</div>
                    </div>
                </div>
            </div>
        </div>
        <?php include ("inc/footer.php"); ?>
    </div>

	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/wow.min.js"></script>
	<script src="js/jquery-price-slider.js"></script>
	<script src="js/jquery.meanmenu.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/jquery.sticky.js"></script>
	<script src="js/jquery.scrollUp.min.js"></script>
	<script src="js/counterup/jquery.counterup.min.js"></script>
	<script src="js/counterup/waypoints.min.js"></script>
	<script src="js/counterup/counterup-active.js"></script>
	<script src="js/scrollbar/jquery.mCustomScrollbar.concat.min.js"></script>
	<script src="js/scrollbar/mCustomScrollbar-active.js"></script>
	<script src="js/metisMenu/metisMenu.min.js"></script>
	<script src="js/metisMenu/metisMenu-active.js"></script>
	<script src="js/sparkline/jquery.sparkline.min.js"></script>
	<script src="js/sparkline/jquery.charts-sparkline.js"></script>
	<script src="js/sparkline/sparkline-active.js"></script>
	<script src="js/plugins.js"></script>
	<script src="js/main.js"></script>
	<script type="text/javascript" src="js/underscore/underscore-min.js"></script>
	<script type="text/javascript" src="js/jstimezonedetect/jstz.min.js"></script>
	<script type="text/javascript" src="js/language/bg-BG.js"></script>
	<script type="text/javascript" src="js/language/nl-NL.js"></script>
	<script type="text/javascript" src="js/language/fr-FR.js"></script>
	<script type="text/javascript" src="js/language/de-DE.js"></script>
	<script type="text/javascript" src="js/language/el-GR.js"></script>
	<script type="text/javascript" src="js/language/it-IT.js"></script>
	<script type="text/javascript" src="js/language/hu-HU.js"></script>
	<script type="text/javascript" src="js/language/pl-PL.js"></script>
	<script type="text/javascript" src="js/language/pt-BR.js"></script>
	<script type="text/javascript" src="js/language/ro-RO.js"></script>
	<script type="text/javascript" src="js/language/es-CO.js"></script>
	<script type="text/javascript" src="js/language/es-MX.js"></script>
	<script type="text/javascript" src="js/language/es-ES.js"></script>
	<script type="text/javascript" src="js/language/es-CL.js"></script>
	<script type="text/javascript" src="js/language/es-DO.js"></script>
	<script type="text/javascript" src="js/language/ru-RU.js"></script>
	<script type="text/javascript" src="js/language/sk-SR.js"></script>
	<script type="text/javascript" src="js/language/sv-SE.js"></script>
	<script type="text/javascript" src="js/language/zh-CN.js"></script>
	<script type="text/javascript" src="js/language/cs-CZ.js"></script>
	<script type="text/javascript" src="js/language/ko-KR.js"></script>
	<script type="text/javascript" src="js/language/zh-TW.js"></script>
	<script type="text/javascript" src="js/language/id-ID.js"></script>
	<script type="text/javascript" src="js/language/th-TH.js"></script>
	<script type="text/javascript" src="js/calendar.js"></script>
	<script type="text/javascript" src="js/app.js?v=1"></script>
</body>

</html>