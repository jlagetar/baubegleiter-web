<?php
	include('_database.php');
	include('_session.php');
	include('functions.php');
	
	$is_admin = false;
	
	if($_SESSION["user_is_admin"] == 1) {
		$is_admin = true;
	}
?>

<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Baubegleiter Startseite</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php include("inc/header.php"); ?>
</head>

<body>
    <!--[if lt IE 8]>
		<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
	<![endif]-->
    <!-- Start Left menu area -->
    <? include ("inc/left_menu.php"); ?>
    <!-- End Left menu area -->
    <!-- Start Welcome area -->
    <div class="all-content-wrapper">
		<?php include("inc/top_menu.php"); ?>
        <div class="analytics-sparkle-area">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12" style="text-align:center;">
						<h1 style="padding-top:2em;padding-bottom:2em;">Startseite des Baubegleiters</h1>
						<p>Herzlich Willkommen!<br/><br/>In dem Login Bereich kannst du alle Zeitbuchungen im Kalender einsehen oder auch neue durchführen.</p>
                    </div>
                </div>
            </div>
        </div>
        <?php include ("inc/footer.php"); ?>
    </div>

    <?php include ("inc/scripts.php"); ?>
</body>

</html>