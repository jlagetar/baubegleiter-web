<?php 
	session_cache_limiter('none');
	if (!isset($_SESSION)) {
    	session_start();
	}
	
	session_destroy();
	header("Location: index.php");
	exit();
?>