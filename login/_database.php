<?php
	$DB_NAME = 'baubegleiter';
	$DB_USER = 'lageta_6';
	$DB_PASSWORD = 'UP1JACpPBthzPxp4';
	$DB_HOST = 'sql561.your-server.de';

	$conn = new mysqli($DB_HOST, $DB_USER, $DB_PASSWORD, $DB_NAME);

	// Check connection
	if ($conn->connect_error) {
		die("Fehler bei Datenbankverbindung: " . $conn->connect_error);
	}
	
	$getUserIDFromTokenFunction = function($token) use ($conn) {
		$get_user_id_sql = "SELECT user_id FROM user_token WHERE token='".$token."' AND created > TIMESTAMP(DATE_SUB(NOW(), INTERVAL 1 day));";
				
		if ($get_user_id = $conn->query($get_user_id_sql)) {
			$user_id = $get_user_id->fetch_assoc();
			if ($user_id != null && $user_id['user_id'] != null) {
				return $user_id['user_id'];
			}
		}
		
		return 0;
	}
?>