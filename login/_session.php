<?php
	session_cache_limiter('none');
	if (!isset($_SESSION)) {
    	session_start();
	}
	
	if (!isset($_SESSION["user_id"]) || $_SESSION["user_id"] == null) {
		header("Location: index.php?session-timeout=1");
		exit();
	}
?>